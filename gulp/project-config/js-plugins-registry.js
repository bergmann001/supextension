module.exports = function() {
    var pluginDefinitions = {
        'angular-1.4-main': {
            dirName: 'angular',
            fileName: 'angular-1.4.js',
            version: '1.4.3'
        },
        'angular-1.4-animate': {
            dirName: 'angular',
            fileName: 'angular-animate-1.4.js',
            version: '1.4.3'
        },
        'angular-1.4-aria': {
            dirName: 'angular',
            fileName: 'angular-aria-1.4.js',
            version: '1.4.3'
        },
        'angular-1.4-route': {
            dirName: 'angular',
            fileName: 'angular-route-1.4.js',
            version: '1.4.3'
        },
        'angular-1.4-package': {
            isPackage: true,
            set: [
                'angular-1.4-main',
                'angular-1.4-animate',
                'angular-1.4-aria',
                'angular-1.4-route'
            ]
        },    	
    	'angular-1.3-main': {
            dirName: 'angular',
            fileName: 'angular-1.3.js',
            version: '1.3.16'
        },
    	'angular-1.3-animate': {
            dirName: 'angular',
            fileName: 'angular-animate-1.3.js',
            version: '1.3.16'
        },
    	'angular-1.3-package': {
    		isPackage: true,
    		set: [
    			'angular-1.3-main',
    			'angular-1.3-animate'
    		]
    	},
        'angular-material': {
            dirName: 'angular',
            fileName: 'angular-material.js',
            version: '0.10.1-rc5'
        },
        'fastclick': {
            dirName: 'fastclick',
            fileName: 'fastclick-106.js',
            version: '1.0.6'
        },
        'jquery-1': {
        	dirName: 'jquery-1',
        	fileName: 'jquery-1.11.3.js',
        	version: '1.11.3'
        },
        'jquery-2': {
        	dirName: 'jquery-2',
        	fileName: 'jquery-2.1.4.js',
        	version: '2.1.4'
        },
        'zepto': {
        	dirName: 'zepto',
        	fileName: 'zepto.js',
        	version: '1.1.6'
        },
        'hammerjs': {
        	dirName: 'hammerjs',
        	fileName: 'hammer-2.0.4.js',
        	version: '2.0.4'	
        },
        'imagefill-src': {
        	dirName: 'imagefill',
        	fileName: 'hammer.js',
        	version: 'default'
        },
        'imagesloaded': {
        	dirName: 'imagesloaded',
        	fileName: 'imagesloaded.js',
        	version: '3.1.8'
        },
        'imagefill': {
            isPackage: true,
    		set: ['imagesloaded', 'imagefill-src']
        },
        'jquery-matchheight': {
        	dirName: 'jquery-matchheight',
        	fileName: 'matchheight.js',
        	version: 'default'
        },
        'twojs': {
        	dirName: 'twojs',
        	fileName: 'two.js',
        	version: '1.5.1'
        },
        'backbone': {
        	dirName: 'backbonejs',
        	fileName: 'backbone.js',
        	version: '1.2.1'
        },
        'react': {
        	dirName: 'react',
        	fileName: 'react.js',
        	version: '0.13.3'
        },
        'd3': {
        	dirName: 'd3js',
        	fileName: 'd3.js',
        	version: '3.5.6'	
        },
        'isjs': {
        	dirName: 'isjs',
        	fileName: 'is.js',
        	version: '0.7.4'	
        },
        'lodash-compat': {
        	dirName: 'lodash',
        	fileName: 'lodash-compat.js',
        	version: '3.10.0',
        	comment: 'Version with support for older browsers like IE8'
        },
        'lodash': {
        	dirName: 'lodash',
        	fileName: 'lodash',
        	version: '3.10.0'
        },
        'pouchdb': {
        	dirName: 'pouchdb',
        	fileName: 'pouchdb-3.6.0.js',
        	version: '3.6.0'	
        },
        'selectordie': {
        	dirName: 'selectordie',
        	fileName: 'selectordie.js',
        	version: 'default'	
        },
        'momentjs': {
        	dirName: 'momentjs',
        	fileName: 'moment.js',
        	version: '2.10.3'	
        },
        'modernizr': {
        	dirName: 'modernizr',
        	fileName: 'modernizr.custom.js'	
        }
    };
    
    
    return pluginDefinitions;
};
