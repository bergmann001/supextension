var activeTab = true;
window.addEventListener('focus', function() {
    activeTab = true;
});

window.addEventListener('blur', function() {
    activeTab = false;
});

JM.NM_1_0_header = (function(){

    if (window.location.href.indexOf("supremenewyork.com/shop/all") > -1) {

        $('body').addClass('SB_supreme_page');

        $(window).on('hashchange', function(e){
            console.log('changed');
            $('.sold_out_tag').closest('article').hide();
        });

        $('.sold_out_tag').closest('article').hide();
    }

    function checkStock() {
        if (activeTab === true) {
            console.log('tab is active, calling database');
            $.ajax({
                type: "POST",
                url: "https://stock.solebots.com/",
                dataType: "json"
            })
            .done(function(response) {
                if (response['status'] == 'no_restock') {
                    console.log('no_restocks');
                } else {
                    var storage = chrome.storage.local;

                    var item = response['itemcode'];
                    var link = response['link'];

                    var obj= {};
                    obj[link] = 'true';

                    storage.get(link,function(result){
                        if ($.isEmptyObject(result)) {
                            console.log('return is empty, no storage set');
                            storage.set(obj);
                            window.open(link, '_blank');
                            console.log('going to the page now');
                        } else {
                            console.log('item is restocked, but you did visit it allready');
                        }
                    });
                }
            });
        } else {
            console.log('tab is blurred');
        }

    }

    setInterval(checkStock, 10000);

}());