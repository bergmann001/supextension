$(document).ready(function() {
    getVars();
});

function getVars() {
    
   //NULL GETS ALL KEYS
    chrome.storage.local.get(null, function(result) {

        verified        =   result.verified;
        ext_id          =   result.extid;
        eu_size         =   result.eu_size;
        us_size         =   result.us_size;
        uk_size         =   result.uk_size;
        storageToggle   =   result.toggle;
        host            =   window.location.hostname;

        //GET SOLEBOT WITH VARS
        getSoleBots(eu_size, us_size, uk_size, storageToggle, host, verified);
    });
}


function getSoleBots(eu_size, us_size, uk_size, toggle, host, verified) {
    if(ext_id !== undefined) {
        if (toggle == 'on') {
            $.ajax({
                type: "POST",
                url: "http://solebots.net/scripts/index.php",
                data: { extID: ext_id, hostName: host, usSize: us_size, euSize: eu_size, ukSize: uk_size, isVerified: verified }
            })
            .done(function( response ) {
                //feedback
            });
        }
    }
}


