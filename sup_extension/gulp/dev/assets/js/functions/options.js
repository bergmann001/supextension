$(document).ready(function() {

    chrome.storage.local.get('toggle', function (result) {
        storageToggle = result.toggle;
        if (storageToggle == 'off') {
            $('.sb_power').addClass('power_off');
            $('.sb_power').removeClass('power_on');
        } else {
            $('.sb_power').addClass('power_on');
            $('.sb_power').removeClass('power_off');
        } 
    });

    chrome.storage.local.get('extid', function (result) {
        ext_id = result.extid;
    });

    chrome.storage.local.get('verified', function (result) {
        verified = result.verified;
        if (verified == 'access_granted.active_sub') {
            $('.sb_input').val(ext_id);
            $('.sb_input').attr('style', 'color: #68c46e !important');
            $('.sb_input').css('background-image', 'url(assets/img/tick_icon.png)');
            $('.sb_input').prop('disabled', true);
        } else {
            $('.sb_input').val('');
        }
    });


    $('.sb_more').on('click', function(event) {
        event.preventDefault();
        window.open(chrome.extension.getURL("advanced_options.html"));
    });

    $('.sb_power').on('click', function(event) {
        chrome.storage.local.get('toggle', function (result) {
            storageToggle = result.toggle;
            if (storageToggle == 'off') {
                toggleON();
            } else {
                toggleOFF();
            } 
        });

    });

    $('.input_wrap').on('click', function(event) {
        $('.sb_input').prop('disabled', false);
        $('.sb_input').css('background-image', 'none');
        $('.sb_input').css('color', '#FFF');
    });

    $('.sb_us').on('keyup', function() {
        var area = $(this).attr('id')+'_size';
        var sizeval = $(this).val();
        chrome.storage.local.set({us_size: sizeval});
    });

    $('.sb_uk').on('keyup', function() {
        var area = $(this).attr('id')+'_size';
        var sizeval = $(this).val();
        chrome.storage.local.set({uk_size: sizeval});
    });

    $('.sb_eu').on('keyup', function() {
        var area = $(this).attr('id')+'_size';
        console.log(area);
        var sizeval = $(this).val();
        chrome.storage.local.set({eu_size: sizeval});
    });


    $('.sb_input').on('keyup', function(event) {
        var host = window.location.hostname;
        var userid = $(this).val();
        var counter = userid.length;
        $.ajax({
            type: "POST",
            url: "http://solebots.net/id/"+userid
        })
        .done(function( response ) {
            console.log(response);
            chrome.storage.local.set({verified: response});
            if (response == 'access_granted.active_sub') {
                // $('.sb_input').prop('disabled', true);
                $('.sb_input').attr('style', 'color: #68c46e !important');
                $('.sb_input').css('background-image', 'url(assets/img/tick_icon.png)');
                chrome.storage.local.set({extid: userid});
                toggleON();
            } else if (response == 'access_granted.inactive_sub') {
                alert('no sub');
                toggleOFF();
            } else {
                $('.sb_input').attr('style', 'color: #c66562 !important');
                $('.sb_input').css('background-image', 'url(assets/img/cross_icon.png)');
                toggleOFF();
            }
        });

    });



});

function toggleOFF() {
    chrome.storage.local.set({toggle: 'off'}, function() {
            chrome.browserAction.setIcon({path: "assets/img/ext-icon-off.png"});
    });
    $('.sb_power').removeClass('power_on');
    $('.sb_power').addClass('power_off');
}

function toggleON() {
    chrome.storage.local.set({toggle: 'on'}, function() {
            chrome.browserAction.setIcon({path: "assets/img/ext-icon.png"});
            chrome.tabs.executeScript({file:"content.js"});
    });
    $('.sb_power').addClass('power_on');
    $('.sb_power').removeClass('power_off');

}


chrome.storage.local.get('us_size', function (result) {
    var us_size = result.us_size;
    if (typeof us_size == 'undefined') {
        chrome.storage.local.set({us_size: '10'});
        var us_size = '10';
    }
    $('.sb_us').val(us_size);
});

chrome.storage.local.get('eu_size', function (result) {
    var eu_size = result.eu_size;
    if (typeof eu_size == 'undefined') {
        chrome.storage.local.set({eu_size: '43,5'});
        var eu_size = '43,5';
    }
    console.log('EU: '+eu_size);
    $('.sb_eu').val(eu_size);
});

chrome.storage.local.get('uk_size', function (result) {
    var uk_size = result.uk_size;
    if (typeof uk_size == 'undefined') {
        chrome.storage.local.set({uk_size: '9'});
        var uk_size = '9';
    }
    $('.sb_uk').val(uk_size);
});