var activeTab = true;
window.addEventListener('focus', function() {
    activeTab = true;
});

window.addEventListener('blur', function() {
    activeTab = false;
});

JM.NM_1_0_header = (function(){

    if (window.location.href.indexOf("supremenewyork.com/shop/all") > -1) {

        $('body').addClass('SB_supreme_page');

        $(window).on('hashchange', function(e){
            console.log('changed');
            $('.sold_out_tag').closest('article').hide();
        });

        $('.sold_out_tag').closest('article').hide();
    }

    function checkStock() {
        if (activeTab === true) {
            console.log('tab is active, calling database');
            $.ajax({
                type: "POST",
                url: "https://stock.solebots.com/",
                dataType: "json"
            })
            .done(function(response) {
                if (response['status'] == 'no_restock') {
                    console.log('no_restocks');
                } else {
                    var storage = chrome.storage.local;

                    var item = response['itemcode'];
                    var link = response['link'];

                    var obj= {};
                    obj[link] = 'true';

                    storage.get(link,function(result){
                        if ($.isEmptyObject(result)) {
                            console.log('return is empty, no storage set');
                            storage.set(obj);
                            window.open(link, '_blank');
                            console.log('going to the page now');
                        } else {
                            console.log('item is restocked, but you did visit it allready');
                        }
                    });
                }
            });
        } else {
            console.log('tab is blurred');
        }

    }

    setInterval(checkStock, 10000);

}());
JM.NM_4_0_footer = (function(){
	
}());
/**
* @desc:
* init singleton base-class (connect to backend controller via JM = JMG.getPrivateNs();
*/
JM.register('base', [':load'], function(){
	var base = function(config){
		//---------------------------------------------------------
		//define __construct
		constructor : JM.base;
		//---------------------------------------------------------
		//__construct object
		JM.base = function(){
			return(_instance);
		}
		//---------------------------------------------------------
		//internal private singleton object returned with new
		var _instance = {
			//-----------------------------------------------------
			//membervars (internal params)
			//-----------------------------------------------------
			/**
			 * @desc:
			 * member vars
			 */
			_params:{},
			//-----------------------------------------------------
			//class methods
			//-----------------------------------------------------
			/**
			* @desc:
			* check if param exists
			*/
			isset: function(varName){
				if(this._params[varName] && this._params[varName] !== 'undefined'){
					return true;
				}
				return false;
			},
			/**
			* @desc:
			* sets param (or a object)
			*/
			set: function(varName,varData){
				if(typeof varName !== 'undefined'){
					if(typeof varName === 'object'){
						for(var key in varName){
							this._params[key] =  varName[key];
						}
					}
					else{
						this._params[varName] = varData;
					}
				}
			return this;
			},
			/**
			* @desc
			* helper returns param
			*
			* @input-required
			* @var varName {object}
			*
			* @input-optional
			* @var varName {object}
			*
			* @return {int}
			*/
			get: function(varName, _default){
				if(typeof varName === 'undefined'){
					return this._params
				}
				else if(typeof this._params[varName] !== 'undefined'){
					return this._params[varName];
				}
				else if(typeof this._params[varName] === 'undefined' && typeof _default !== 'undefined'){
					return _default;	
				}
			},
			/**
			* @desc
			* helper counts objects
			*
			* @input-required
			* @var obj {object}
			*
			* @return {int}
			*/
			count: function(obj){
				var _size = 0;
				if(typeof obj == 'object'){
					for(var key in obj) {
						if (obj.hasOwnProperty(key)){
							_size++;
						}
					}
				}
				return _size;
			},
			/**
			* @desc
			* helper checks if array/object the the passed value objects
			*
			* @input-required
			* @var needle {string}
			* @var haystack {object}
			*
			* @return {int}
			*/
			inObject: function(needle, haystack) {
				if(typeof haystack == 'object'){
					for(var i in haystack){
						if(haystack[i] === needle) return true;		
					}
				}
				else{
					var length = haystack.length;
					for(var i = 0; i < length; i++){
						if(typeof haystack == 'object'){
							alert(haystack[i]);	
						}
						if(haystack[i] === needle) return true;
					}
				}
				return false;
			}
		}
		//---------------------------------------------------------
		//init with config on first call
		_instance.set(config);
		//-------------------------------------------------------------
	return(_instance);
	};
	//init base
	return new base();
});



JM.SB_background = (function(){
	
}());

chrome.storage.local.get('verified', function (result) {
    verified = result.verified;
    console.log(verified);
        console.log('true');

    if (verified == 'access_denied.banned') {
        console.log('true');
        $('.SB_MOD_NM_1-0-header').addClass('SB_st-error');
        $('.SB_digit-wrap').hide();
        setStatus('Sorry','You’re temporarily blocked because you tried to login with a wrong access key too many times. Come back in 5 minutes.');
        // if (banCheck === false) {
        //     cleanUp();
        // }
    } else if (verified == 'access_granted.active_sub') {
        $('.SB_burger-ct').addClass('SB_st-logged-in');
        setStatus('Welcome back','Your solebot is ready to go. If you want to change your settings go to the menu.');
        $('.SB_digit-wrap').hide();
    } else {
        $('.SB_first').focus();
    }

});

