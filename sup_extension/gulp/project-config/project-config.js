var BuildConfig = require('../dev/_gulp-ext/gulp-config.js');
var jsplugins_registry = require('./js-plugins-registry.js')();

function eachParser(cont) {
    var eachOccurence = cont.match(/@each\([a-z]+\:[a-z0-9\-]+(,[a-z0-9\-]+)*\)/gi);

    var esc = function(s) {
        return s.replace('(','\\(').replace(')','\\)');
    };
    for (var eo in eachOccurence) {
        var eachExpr = eachOccurence[eo];
        var exprPart = eachExpr.substring('@each('.length);
        exprPart = exprPart.substring(0,exprPart.length - 1);
        var splittedExpr = exprPart.split(':');
        var exprName = splittedExpr[0];
        var toEach = splittedExpr[1].split(',');
        var endExpr = '@eachEnd('+exprName+')';

        var findEndExpr = cont.match(new RegExp(esc(endExpr), 'gi'));
        var startIndex  = cont.indexOf(eachExpr);
        var endIndex    = cont.indexOf(endExpr);

        if (startIndex > 0 && endIndex>0) {
            var partAbove = cont.slice(0, startIndex);
            var parsingPart = cont.slice(startIndex+eachExpr.length, endIndex);

            var htmlToSet = '';
            var repString = '@('+exprName+')';
            for (var i in toEach) {
                //speak: for every element given in @each(NAME: elem1, elem2, elem3)...
                var currVal = toEach[i];
                htmlToSet += parsingPart.replace(new RegExp(esc(repString),'gi'), currVal);
            }

            htmlToSet = eachParser(htmlToSet);

            var partBelow = cont.slice(endIndex+endExpr.length);

            cont = partAbove + htmlToSet + partBelow;
        }
    }


    return cont;
}



/**
 * 1. define development path (if it differs)
 */
var devDirName = 'dev';
/**
 * 2. define application path (if it differs)
 */
var appDirName = '../htdocs';
/**
 * 3. define global namespace for CSS/JS and filename prefixes
 */
var projectNamespace = 'JM_';
/**
 * 4. if you had made changes in 3),replace global namespace manually in all .scss files (via search and replace)
 */

/**
 * [exports description]
 */
module.exports =
    BuildConfig
        .devFrom('./'+devDirName)
            .assetsFrom('assets')
            .objectsFrom('objects')
            .end()
        .buildAt('./'+appDirName)
                .assetsAt('assets')
                .assets_imagesAt('img')
            .end()
        .fileVariables([
            {'search': 'MODULE_ELEMENTS_BASE', replace: process.cwd()+'/'+devDirName+'/modules'},
            {'search': 'COPYRIGHT_INFO', replace: '2016, Solebots'},
            {'search': 'PROJECT_WEB_URL', replace: 'www.solebots.com'},
            {'search': 'PROJECT_WEB_TITLE', replace: 'Solebots Supreme'},
            {'search': 'PROJECT_NAMESPACE', replace: projectNamespace},
            {'search': 'CHANGE_DATE', replace: new Date()}
        ])
        .localVar('PROJECT_NAMESPACE', projectNamespace)
        .localVar('AUTO_RETINA_IMAGES', true) //use `brew install graphicsmagick` to use this!
        .localVar('CREATE_INLINEICONS_CSS', false)
        .localVar('GLOBAL_JS_NAME', 'global.js')
        .localVar('GLOBAL_SASS_NAME', 'global.scss')
        .localVar('GLOBAL_SASS_ENHANCED_NAME', 'global-enhanced.scss')
        .localVar('NO_NG_EXPRESSION_REPLACEMENT', false) //set to true when using angular {{expr}}
        .inclusionFilter(function(content,filePath,cwd){
            var relPath = filePath.replace(cwd,'');
            content = eachParser(content);
            if (relPath.replace('/objects/')==relPath) {
                //is module
                return '<!-- startModule @PATH='+relPath+' -->' + content;
            }

            return content;
        })
        .jsplugins()
            .useJsRegistry(jsplugins_registry)
            .add('modernizr',true,'vendor')
            .add('fastclick',true,'vendor')
            .add('jquery-1')
        .end();
