#!/bin/bash
npm i
BREW_LOCATION=$(which brew)
GRAPHICKSMAGICK_LOCATION=$(which gm)

if [ "$BREW_LOCATION" == "" ]; then
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# http://stackoverflow.com/questions/17756587/installing-graphicsmagick-on-mac-os-x-10-8
# http://developpeers.com/blogs/fix-for-homebrew-permission-denied-issues


# if you do not have brew pre-installed, do the following if errors occur:
# brew uninstall $(brew list)
# brew cleanup -s

brew update
brew upgrade
brew cleanup -s
brew install imagemagick 
brew install graphicsmagick  

GRAPHICKSMAGICK_LOCATION=$(which gm)

if [ "$GRAPHICKSMAGICK_LOCATION" == "" ]; then
    echo "---------------"
    echo "---------------"
    echo "Graphicksmagick is not installed!"
    echo "!! Make sure to revisit this ressource and follow the instructions: https://www.npmjs.com/package/gulp-gm"
fi